#!/bin/sh

INSTALL=${INSTALL:-"cp -pR"}
MKDIR=${MKDIR:-"mkdir -p"}
DESTDIR=${DESTDIR:-"${HOME}/rrr"}
PREFIX=${PREFIX:-} 
P=${P:-"rcr"}
DOCDIR=${DOCDIR:-"share/doc/${P}"}

if [ -f conf/config.sh ] ; then
    . conf/config.sh
fi

install_general()
{
    args="$@"
    set -o xtrace
    ${MKDIR} "${DESTDIR}${PREFIX}/${owndir}"
    ${INSTALL} $args "${DESTDIR}${PREFIX}/${owndir}"
    set +o xtrace

}

install_bin()
{
    owndir='bin'
    install_general $owndir'/rcr'
    unset owndir
}
install_doc()
{
    owndir="${DOCDIR}"
    install_general 'README-rcr' 'doc/howto-rcr.html' 'doc/howto-rcr.txt'
    unset owndir
}

set -o errexit

install_bin
install_doc
